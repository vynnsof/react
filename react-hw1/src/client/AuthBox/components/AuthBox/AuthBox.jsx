import React from 'react';
import './AuthBox.css';

export const AuthBox = () => {
  return (
    <div className="form-inline">
      <button className="btn btn-outline-light" type="button">Sign in</button>
      <button className="btn btn-info" type="button">SSign Up</button>
    </div>
  );
};