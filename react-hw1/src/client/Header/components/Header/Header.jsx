import React from 'react';

import './Header.css';

import {Navbar} from '../../../Navbar/components/Navbar';

export const Header = () => {
return(
  <header className="header d-flex justify-content-between align-items-center row">
    <Navbar/>

  </header>
);
}