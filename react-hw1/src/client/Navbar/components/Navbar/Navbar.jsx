import React from 'react';
import './Navbar.css';

import { Logo } from '../../../Logo/components/Logo';
import { AuthBox } from '../../../AuthBox/components/AuthBox';

export const Navbar = () => {
  return (
    <nav className="navbar sticky-top">
      <Logo/>
      <AuthBox/>
      {/*<form className="form-inline">*/}
      {/*  <button className="btn btn-outline-success" type="button">Main button</button>*/}
      {/*  <button className="btn btn-sm btn-outline-secondary" type="button">Smaller button</button>*/}
      {/*</form>*/}
    </nav>
  );
};